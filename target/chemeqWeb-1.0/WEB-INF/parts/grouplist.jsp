<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${groupManager.hasGroups}">
	<h:form>
		<hr />
		<h3>List of already added groups:</h3>
		<h:dataTable value="#{groupManager.addedGroups}" rowClasses="oddrow,evenrow" var="row">
			<h:column/><h:column/><h:column>
				<h:outputText value="#{row.name}"/>
			</h:column>
			<h:column/><h:column><h:outputText value="="/></h:column><h:column/>
			<h:column>
				<h:outputText value="#{row.value}"/>
			</h:column>
		</h:dataTable>
	</h:form>
</c:if>