<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
   <f:view>
      <head>
         <link href="${pageContext.request.contextPath}/chem.css" rel="stylesheet" type="text/css"/>
         <title>Chemical equation solver</title>
      </head>
      <body background="${pageContext.request.contextPath}/images/background-blue.gif">
         <h3>Chemical Equation Solver: Add group or pseudo element.</h3>
         <jsp:include page="/WEB-INF/parts/grouplist.jsp"/>
         <h:form>
            <c:if test="${groupManager.errorMsg != ''}">
				<h3 class="errormsg"><h:outputText value="#{groupManager.errorMsg}"/></h3>      
	            <br />
	        </c:if>
            <hr /><br />
            <h3>Enter symbol and value:</h3>
			<h:inputText value="#{groupManager.groupName}" styleClass="element"/>&nbsp;=&nbsp;
			<h:inputText value="#{groupManager.groupValue}" styleClass="compound"/>&nbsp;
            <br /><br />
   			<h:commandButton value="Add Group" action="#{groupManager.doAddGroup}"/>&nbsp;
    		<h:commandButton value="Back" action="#{groupManager.doBack}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   			<h:commandButton value="Clear All" action="#{groupManager.doClearGroup}"/>
         </h:form>
      </body>
   </f:view>
</html>