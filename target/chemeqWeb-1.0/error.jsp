<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
     <head>
        <link href="${pageContext.request.contextPath}/chem.css" rel="stylesheet" type="text/css"/>
        <title>Chemical equation solver</title>
     </head>
     <body background="${pageContext.request.contextPath}/images/background-blue.gif">
       <h3>Chemical equation solver: <span class="errormsg">Session timed out or invalidated</span></h3>
       <hr />
	   <h3>
	       Click <a href="${pageContext.request.contextPath}/eq.jsf">here</a> to start a new session.
	   </h3>
     </body>
</html>
