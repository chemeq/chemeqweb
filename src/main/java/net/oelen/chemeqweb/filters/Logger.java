package net.oelen.chemeqweb.filters;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.oelen.chemeqweb.beans.Calculator;
import net.oelen.chemeqweb.beans.ChemRow;





class FileLogger {
	FileWriter out;
	
	public FileLogger(String filename) {
		try {
			out = new FileWriter(filename, true);
		}
		catch (IOException e) {
			out = null;
		}
	}
	
	
	
	public void log(String s) {
		synchronized(this) {
			if (out != null) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
					out.write(df.format(new Date()) + "  ");
					out.write(s);
					out.write('\n');
					out.flush();
				}
				catch (IOException e) {
					try {out.close();} catch (Exception ignore) { }
					out = null;
				}
			}
		}
	}
}





/**
 * Servlet Filter implementation class Logger
 */
public class Logger implements Filter {
	
	private FilterConfig config = null;
	private FileLogger flogger;
	
   

	
    public Logger() {
    }
    


    
    
	public void destroy() {
		flogger = null;
		config = null;
	}


	
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
	                     throws IOException, ServletException {

		// We have a HTTP servlet request.
		HttpServletRequest req = (HttpServletRequest)request;
		
		// Determine whether the session is started or not.
		HttpSession sess = req.getSession();
		Calculator calcBean = (Calculator)sess.getAttribute("calculator");
		String sessionStarted = (calcBean == null) ? "session started " : "";
		
		// pass the request along the filter chain
		chain.doFilter(request, response);
		
		// Do the logging only after fulfilling the request. This is done, because
		// after fulfilling the request more info is available at what the user
		// has done in the beans and no elaborate request processing is required here.
		
		if (flogger != null) {
			// Log the request to the log file.
			
			String src = req.getRemoteHost();
			String path = req.getRequestURI();
			if (path == null) {
				path = "";
			}
			
			calcBean = (Calculator)sess.getAttribute("calculator");
			String compounds = "NO CALCULATOR BEAN";
			if (calcBean != null) {
				compounds = "";
				ChemRow[] rows = calcBean.getChemrows();
				int nIn = 0;
				for (int i=0; i<rows.length; i++) {
					if (rows[i].getIn() != null && !"".equals(rows[i].getIn())) {
						compounds = compounds + ((nIn == 0) ? rows[i].getIn() : ", " + rows[i].getIn());
						nIn++;
					}
				}
				int nOut = 0;
				for (int i=0; i<rows.length; i++) {
					if (rows[i].getOut() != null && !"".equals(rows[i].getOut())) {
						compounds = compounds + ((nOut == 0) ? " | " + rows[i].getOut() : ", " + rows[i].getOut());
						nOut++;
					}
				}
			}
			
			String total = sessionStarted + compounds;
			if ("".equals(total)) {
				flogger.log(src + ": " + path);
			}
			else {
				flogger.log(src + ": " + path + " : " + total);
			}
		}
	}

	
	
	
	public void init(FilterConfig fConfig) throws ServletException {
		config = fConfig;
		
		ServletContext application = config.getServletContext();
		synchronized(application) {
			Object attr = application.getAttribute("flogger");
			if (attr == null || !(attr instanceof FileLogger)) {
				// Obtain the name of the file where we should log.
				// If this starts with a /, then it is regarded as
				// a full path, relative to the root of the file
				// system. If it does not start with a /, then we
				// determine the path, relative to the app-context.
				String filename = config.getInitParameter("logfile");
				if (filename == null || "".equals(filename.trim()) || "/".equals(filename.trim())) {
					filename = "/tmp/chemeq.log";
				}
				filename = filename.trim();
				if (filename.charAt(0) != '/') {
					filename = application.getRealPath("/"+filename);
				}
				
				// Open a new logger with the file name.
				flogger = new FileLogger(filename);
				application.setAttribute("flogger", flogger);
			}
			else {
				flogger = (FileLogger)attr;
			}
		}
	}

}
