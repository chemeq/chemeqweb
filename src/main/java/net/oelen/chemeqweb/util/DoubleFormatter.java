package net.oelen.chemeqweb.util;



public class DoubleFormatter {
	
	// A special formatter, suited for chemistry related masses. Such masses usually are
	// larger than 1, but lower than a few thousands. For electrons only, the masses are
	// in the range of 0.0005.
	public static String format(double val)  {
		String s = "";
		String sign = "";
		if (val < 0) {
			val = -val;
			sign = "-";
		}
		
		if (val >= 5000) {
			val = val + 0.5;
			s = s + val;
			int n1 = s.indexOf('.');
			if (n1 > 0) {
				s = s.substring(0, n1);
			}
		}
		else if (val >= 500) {
			val = val + 0.05;
			s = s + val;
			int n1 = s.indexOf('.');
			if (n1 < 0) {
				s = s + ".00";
				n1 = s.indexOf('.');
			}
			else {
				s = s + "00";
			}
			s = s.substring(0, n1+2);
		}
		else if (val >= 50) {
			val = val + 0.005;
			s = s + val;
			int n1 = s.indexOf('.');
			if (n1 < 0) {
				s = s + ".000";
				n1 = s.indexOf('.');
			}
			else {
				s = s + "000";
			}
			s = s.substring(0, n1+3);
		}
		else if (val >= 5) {
			val = val + 0.0005;
			s = s + val;
			int n1 = s.indexOf('.');
			if (n1 < 0) {
				s = s + ".0000";
				n1 = s.indexOf('.');
			}
			else {
				s = s + "0000";
			}
			s = s.substring(0, n1+4);
		}
		else if (val >= 0.5) {
			val = val + 0.00005;
			s = s + val;
			int n1 = s.indexOf('.');
			if (n1 < 0) {
				s = s + ".00000";
				n1 = s.indexOf('.');
			}
			else {
				s = s + "00000";
			}
			s = s.substring(0, n1+5);
		}
		else if (val == 0.0) {
			s = "0";
		}
		else {
			val = val + 1.0000005;
			s = s + val + "00000000";
			s = "0" + s.substring(1, 7);
		}
		return sign+s;
	}
	
	
	public static double computeFactor(String num, Double den) {
		double n = Double.parseDouble(num);
		return n/den;
	}
}
