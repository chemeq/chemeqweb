package net.oelen.chemeqweb.beans;

public class Group {
	String name;
	String value;
	
	public Group() {name = value = "";}
	public Group(String n, String v) {name = n; value = v;}
	
	public String getName() {return name;}
	public String getValue() {return value;}
	
	public void setName(String s) {name = s;}
	public void setValue(String s) {value = s;}
}
