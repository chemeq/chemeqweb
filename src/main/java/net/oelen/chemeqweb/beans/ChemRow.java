package net.oelen.chemeqweb.beans;


// A simple helper class which allows the calculator bean to communicate 
// with the JSF framework by means of a <h:dataTable> tag. The Calculator
// bean returns an array of ChemRow elements by means of a getter. Elements
// inside this array have a setter and so, the framework is allowed to
// alter individual elements of the array, but the framework is not
// allowed to alter the array itself (e.g. the number of elements cannot
// be altered by the framework).

public class ChemRow {
	private String in, sin, min;
	private String out, sout, mout;
	private int index;
	
	public ChemRow() {
		in = sin = min = "";
		out = sout = mout = "";
		index = 0;
	}
	
	public String getIn() {return in;}
	public String getSin() {return sin;}
	public String getMin() {return min;}
	public String getOut() {return out;}
	public String getSout() {return sout;}
	public String getMout() {return mout;}
	public String getReactantId() {return "reactant[" + index + "]";}
	public String getProductId() {return "product[" + index + "]";}
	
	public void setIn(String s) {in = s.trim();}
	public void setSin(String s) {sin = s.trim();}
	public void setMin(String s) {min = s.trim();}
	public void setOut(String s) {out = s.trim();}
	public void setSout(String s) {sout = s.trim();}
	public void setMout(String s) {mout = s.trim();}
	
	public void setIndex(int index) {this.index = index;}
}
