package net.oelen.chemeqweb.beans;

import javax.faces.event.ActionEvent;
import net.oelen.chemeq.basics.Element;
import net.oelen.chemeq.basics.ElementFactory;
import net.oelen.chemeq.basics.Formula;



// This is a bean, which is used to add pseudo elements or groups
// which need to be preserved in chemical reactions. This bean
// uses the element factory in the HttpSession context to add new
// pseudo elements or groups. It also maintains a simple array
// for presenting the currently added pseudo elements or groups
// by means of a JSF <h:dataTable> tag.

public class GroupManager {

	String groupName;
	String groupValue;
	String errorMsg;
	ElementFactory elemFact;
	
	Group[] addedGroups;
	
	public GroupManager() {
		groupName = "";
		groupValue = "";
		errorMsg = "";
		addedGroups = new Group[0];
		elemFact = null;
	}
	
	public String getGroupName() {return groupName;}
	public String getGroupValue() {return groupValue;}
	public String getErrorMsg() {return errorMsg;}
	public Group[] getAddedGroups() {return addedGroups;}
	public boolean getHasGroups() {return addedGroups != null && addedGroups.length > 0;}
	
	public void setGroupName(String s) {groupName = s.trim();}
	public void setGroupValue(String s) {groupValue = s.trim();}
	public void setElemFact(ElementFactory ef) {elemFact = ef;}
	public ElementFactory getElemFact() {return elemFact;}
	
	public String doAddGroup() {
		errorMsg = "";
		
		// First check the group name. This must be a valid
		// element symbol, which is not yet already in use.
		if (!ElementFactory.isValidElementSymbol(groupName)) {
			errorMsg = "The symbol " + groupName + " is not a valid element symbol, group cannot be added.";
			return "invalidgroup";
		}
		Element elem = elemFact.getElementBySymbol(groupName);
		if (elem != null) {
			errorMsg = "Element " + groupName + " already in use, group cannot be added.";
			return "invalidgroup";
		}
		if ("".equals(groupValue)) {
			errorMsg = "Empty value, group cannot be added.";
			return "invalidgroup";
		}
		
		// Next check the group value. First try to parse it
		// as a floating point number. If this does not
		// succeed, then try to parse it as an isotope of
		// a known element, and if that also fails, then
		// try to parse it as a toplevel chemical formula,
		// containing already known elements.
		double groupMass = -1;
		try {
			groupMass = Double.parseDouble(groupValue);
		}
		catch (Exception e) {
			double mass = isotopeParse(groupValue, elemFact);
			if (mass > 0) {
				groupMass = mass;
			}
			else {
				Formula form = new Formula(groupValue, elemFact);
				int[] elemCount = elemFact.newElemCountArray();
				boolean result = form.parse(elemCount);
				if (result) {
					groupMass = elemFact.totalMass(elemCount);
				}
			}
		}
		if (groupMass <= 0) {
			errorMsg = "Value " + groupValue + " is neither a valid number, nor a valid isotope, nor a valid formula.";
			return "invalidgroup";
		}
		
		Element newElem = elemFact.addPseudoElement(groupName, groupMass);
		if (newElem == null) {
			errorMsg = "Maximum number of pseudo elements is exceeded.";
			return "invalidgroup";
		}
		
		Group[] newAddedGroups = new Group[addedGroups.length + 1];
		for (int i=0; i<addedGroups.length; i++) {
			newAddedGroups[i] = addedGroups[i];
		}
		newAddedGroups[addedGroups.length] = new Group(groupName, groupValue);
		addedGroups = newAddedGroups;
		groupName = groupValue = "";
		return "addgroup";
	}
	
	
	
	public String doClearGroup() {
		groupName = groupValue = "";
		errorMsg = "";
		elemFact.clearPseudoElements();
		for (int i=0; i<addedGroups.length; i++) {
			addedGroups[i] = null;
		}
		addedGroups = new Group[0];
		return "addgroup";
	}
	
	
	
	
	public String doBack() {
		groupName = groupValue = "";
		errorMsg = "";
		return "back";
	}
	
	
/*
	private ElementFactory getElemFact() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession sess = (HttpSession) context.getExternalContext().getSession(false);
		
		ElementFactory elemFact = (ElementFactory)sess.getAttribute("elemFact");
		if (elemFact == null) {
			elemFact = new ElementFactory();
			sess.setAttribute("elemFact", elemFact);
		}
		
		return elemFact;
	}
*/	
	
	
	
	public void reset(ActionEvent e) {
		doClearGroup();
	}
	
	

	
	private static double isotopeParse(String value, ElementFactory elemFact) {
		String[] syms = value.split(":");
		if (syms.length != 2) {
			return -1.0;
		}
		
		syms[0] = syms[0].trim();
		syms[1] = syms[1].trim();
		if ("".equals(syms[0]) || "".equals(syms[1])) {
			return -1.0;
		}
		
		int isotope = 0;
		try {
			isotope = Integer.parseInt(syms[1]);
		}
		catch (Exception e) {
			return -1.0;
		}
		
		if (isotope <= 0) {
			return -1.0;
		}
		
		Element elem = elemFact.getElementBySymbol(syms[0], isotope);
		if (elem == null) {
			return -1.0;
		}
		
		return elem.getMass();
	}
}
