package net.oelen.chemeqweb.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;


public class NumValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		
		if (value == null) {
			return;
		}
		
		if (!(value instanceof String)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					                      "chem.equation.messages", "input must be string");
			throw new ValidatorException(msg);
		}
		
		// We have a string. Extract the non-space part from this.
		String num = ((String)value).trim();
		
		// Allow empty boxes.
		if (num.equals("")) {
			return;
		}
		
		// Do not allow a single "="
		if (num.equals("=")) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "chem.equation.messages", "Assignment must contain numerical value");
			throw new ValidatorException(msg);
		}

		double val = 0.0;
		boolean isAssignment = false;
		try {
			if (num.charAt(0) == '=') {
				isAssignment = true;
				val = Double.parseDouble(num.substring(1).trim());
			}
			else {
				val = Double.parseDouble(num);
			}
		}
		catch (Exception e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "chem.equation.messages",
                    "Invalid ratio or mass value or assignment: " + (String)value);
            throw new ValidatorException(msg);
		}
		
		if (isAssignment && val == 0.0) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "chem.equation.messages", "Assignment must have non-zero value");
			throw new ValidatorException(msg);
		}
	}
}

