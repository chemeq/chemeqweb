package net.oelen.chemeqweb.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpSession;
import net.oelen.chemeq.basics.ElementFactory;
import net.oelen.chemeq.basics.Formula;



public class FormulaValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {

		
		if (value == null || "".equals(value)) {
			return;
		}
		
		if (!(value instanceof String)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					                      "chem.equation.messages", "Inputmust be string");
			throw new ValidatorException(msg);
		}
		
		// Obtain the element factory object.
		HttpSession sess = (HttpSession)context.getExternalContext().getSession(false);
		ElementFactory elemFact = (ElementFactory)sess.getAttribute("elemFact");
		if (elemFact == null) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "chem.equation.messages",
                    "Could not retrieve element factory object from session scope!");
            throw new ValidatorException(msg);
		}

		Formula form = new Formula((String)value, elemFact);
		int[] dummy = elemFact.newElemCountArray();
		boolean result = form.parse(dummy);
		
		if (!result) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    "chem.equation.messages",
                    "Invalid molecule or ion formula: " + (String)value);
            throw new ValidatorException(msg);
		}
	}
}

