<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<html>
   <f:view>
      <head>
         <link href="${pageContext.request.contextPath}/chem.css" rel="stylesheet" type="text/css"/>
         <title>Chemical equation solver</title>
      </head>
      <body background="${pageContext.request.contextPath}/images/background-blue.gif">
         <h:form>
            <h3>Chemical Equation Solver: Enter reactants and products.</h3>
            <hr />
				 <h:messages layout="table" errorClass="errormsg" showSummary="false" showDetail="true"/>
				 <h3 class="errormsg"><h:outputText value="#{calculator.errorMsg}"/></h3>
				 <h:dataTable value="#{calculator.chemrows}" var="row">
				    <h:column>
				        <f:facet name="header"><h:outputText value="Reactants"/></f:facet>
				    	<h:inputText value="#{row.in}" styleClass="compound"/>
				    </h:column>
				    <h:column/>
				    <h:column>
				        <f:facet name="header"><h:outputText value="Ratio"/></f:facet>
				    	<h:inputText readonly="true" value="#{row.sin}" styleClass="ratioro"/>
				    </h:column>
				    <h:column/>
				    <h:column>
				        <f:facet name="header"><h:outputText value="Mass"/></f:facet>
				    	<h:inputText readonly="true" value="#{row.min}" styleClass="massro"/>
				    </h:column>
				    <h:column/><h:column/><h:column/><h:column/><h:column/><h:column/>
				    <h:column>
				        <f:facet name="header"><h:outputText value="Products"/></f:facet>
				    	<h:inputText value="#{row.out}" styleClass="compound"/>
				    </h:column>
				    <h:column/>
				    <h:column>
				        <f:facet name="header"><h:outputText value="Ratio"/></f:facet>
				    	<h:inputText readonly="true" value="#{row.sout}" styleClass="ratioro"/>
				    </h:column>
				    <h:column/>
				    <h:column>
				        <f:facet name="header"><h:outputText value="Mass"/></f:facet>
				    	<h:inputText readonly="true" value="#{row.mout}" styleClass="massro"/>
				    </h:column>
				 </h:dataTable>
				 
				 <p>
				    <h:commandButton value="Compute" action="#{calculator.doCompute}"/>&nbsp;
				    <h:commandButton value="Add Group" action="#{calculator.doSaveAndThenAddGroup}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    <h:commandButton value="Clear" action="#{calculator.doClear}"/>
				 </p>
         </h:form>
		 <p></p>
		 <form method="get" action="${pageContext.request.contextPath}/tutor/tutor.html" target="_blank">
		    <input type="submit" value="Help"/>
		 </form>
         
         <jsp:include page="/WEB-INF/parts/grouplist.jsp"/>

      </body>
   </f:view>
</html>
