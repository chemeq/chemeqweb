<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
   <f:view>
      <head>
         <link href="${pageContext.request.contextPath}/chem.css" rel="stylesheet" type="text/css"/>
         <title>Chemical equation solver</title>
      </head>
      <body background="${pageContext.request.contextPath}/images/background-blue.gif">
         <h:form>
            <h3>Chemical Equation Solver: Solution for given reactants and products</h3>
            <c:if test="${calculator.nresultsets > 1}">
            <h3>
	            <h:outputText value="Warning: Multidimensional solution space, this is result set #{calculator.resultset}." rendered="#{calculator.nresultsets > 1}"/>
	            <br /><span>Click the following for different solutions:</span>
	            <h:commandLink immediate="true" value="1" title="switch to solution set 1" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 1 && calculator.resultset != 1}">
	               <f:param name="resultset" value="1"/>
	            </h:commandLink><h:outputText value="1" rendered="#{calculator.resultset==1}" style="color:#c04040"/>&nbsp;
	            <h:commandLink immediate="true" value="2" title="switch to solution set 2" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 2 && calculator.resultset != 2}">
	               <f:param name="resultset" value="2"/>
	            </h:commandLink><h:outputText value="2" rendered="#{calculator.resultset==2}" style="color:#c04040"/>&nbsp;
	            <h:commandLink immediate="true" value="3" title="switch to solution set 3" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 3 && calculator.resultset != 3}">
	               <f:param name="resultset" value="3"/>
	            </h:commandLink><h:outputText value="3" rendered="#{calculator.resultset==3}" style="color:#c04040"/>&nbsp;
	            <h:commandLink immediate="true" value="4" title="switch to solution set 4" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 4 && calculator.resultset != 4}">
	               <f:param name="resultset" value="4"/>
	            </h:commandLink><h:outputText value="4" rendered="#{calculator.resultset==4}" style="color:#c04040"/>&nbsp;
	            <h:commandLink immediate="true" value="5" title="switch to solution set 5" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 5 && calculator.resultset != 5}">
	               <f:param name="resultset" value="5"/>
	            </h:commandLink><h:outputText value="5" rendered="#{calculator.resultset==5}" style="color:#c04040"/>&nbsp;
	            <h:commandLink immediate="true" value="6" title="switch to solution set 6" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" rendered="#{calculator.nresultsets >= 6 && calculator.resultset != 6}">
	               <f:param name="resultset" value="6"/>
	            </h:commandLink><h:outputText value="6" rendered="#{calculator.resultset==6}" style="color:#c04040"/>
            </h3>
            </c:if>
            <hr />
            
            <%-- TODO: Somewhat inefficient. The equations are recomputed 3 times! --%>
            <c:if test="${not empty calculator.equation}">
            	<h:outputText value="#{calculator.equation[0]} --> #{calculator.equation[1]}" styleClass="equation"/>
            </c:if>
 
			<h:messages layout="table" errorClass="errormsg" showSummary="false" showDetail="true"/>
			<h3 class="errormsg"><h:outputText value="#{calculator.errorMsg}"/></h3>
			        
			<h:dataTable value="#{calculator.chemrows}" var="row">
			    <h:column>
			        <f:facet name="header"><h:outputText value="Reactants"/></f:facet>
			   		<h:inputText readonly="true" value="#{row.in}" styleClass="#{(row.in=='' || row.sin=='0') ? 'compoundro' : 'compoundrolight'}"/>
			    </h:column>
			    <h:column/>
			    <h:column>
			        <f:facet name="header"><h:outputText value="Ratio"/></f:facet>
			    	<h:inputText readonly="#{row.in=='' || row.sin=='0'}" value="#{row.sin}" styleClass="#{row.in=='' || row.sin=='0' ? 'ratioro' : 'ratio'}">
			              <f:validator validatorId="numValidator"/>
			        </h:inputText>
			    </h:column>
			    <h:column/>
			    <h:column>
			        <f:facet name="header"><h:outputText value="Mass"/></f:facet>
			    	<h:inputText readonly="#{row.in=='' || row.sin=='0'}" value="#{row.min}" styleClass="#{row.in=='' || row.sin=='0' ? 'massro' : 'mass'}">
			              <f:validator validatorId="numValidator"/>
			        </h:inputText>
			    </h:column>
			    <h:column/><h:column/><h:column/><h:column/><h:column/><h:column/>
			    <h:column>
			        <f:facet name="header"><h:outputText value="Products"/></f:facet>
			    	<h:inputText readonly="true" value="#{row.out}" styleClass="#{(row.out=='' || row.sout=='0') ? 'compoundro' : 'compoundrolight'}"/>
			    </h:column>
			    <h:column/>
			    <h:column>
			        <f:facet name="header"><h:outputText value="Ratio"/></f:facet>
			    	<h:inputText readonly="#{row.out=='' || row.sout=='0'}" value="#{row.sout}" styleClass="#{row.out=='' || row.sout=='0' ? 'ratioro' : 'ratio'}">
			              <f:validator validatorId="numValidator"/>
			        </h:inputText>
			    </h:column>
			    <h:column/>
			    <h:column>
			        <f:facet name="header"><h:outputText value="Mass"/></f:facet>
			    	<h:inputText readonly="#{row.out=='' || row.sout=='0'}" value="#{row.mout}" styleClass="#{row.out=='' || row.sout=='0' ? 'massro' : 'mass'}">
			              <f:validator validatorId="numValidator"/>
			        </h:inputText>
			    </h:column>
			</h:dataTable>
			        
			<p>
			    <h:commandButton value="Recompute" action="#{calculator.doRecompute}"/>&nbsp;
			    <h:commandButton value="back" actionListener="#{calculator.setResultset}" action="#{calculator.doShowResult}" immediate="true"/>
			</p>
         </h:form>
         
         <jsp:include page="/WEB-INF/parts/grouplist.jsp"/>
         
      </body>
   </f:view>
</html>
