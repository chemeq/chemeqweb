<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>

<html>
   <f:view>
      <head>
         <link href="${pageContext.request.contextPath}/chem.css" rel="stylesheet" type="text/css"/>
         <title>Chemical equation solver</title>
      </head>
      <body background="${pageContext.request.contextPath}/images/background-blue.gif">
        <h:form>
        <h3>Chemical equation solver: <span class="errormsg">Requested resource not available.</span></h3>
        <hr />
		<h3>
		    Click <h:commandLink value="here" actionListener="#{groupManager.reset}" action="#{calculator.doNewSession}"/> to start a new session.
		</h3>
		</h:form>
      </body>
   </f:view>
</html>

